From right-hand panel design:

- how do we get to other assemblies?
- how do we get to previous versions?
- where does the Tark link belong? 
- how to represent Reactome in external references?
- looks like first publication in the list is from NCBI. Do we have links to NCBI as well?
- Which service(s) do we query when we click on the Download (sequence) button
- Previous versions link – what happens when we click on it? Is it taking us to Tark? Is it communicating with Tark
- What do we call the relationship to an SO term? Biotype has a completely different definition in the dictionary to how Ensembl uses it

PROBLEMS:
- No data for publications in Core (at least for human). We don't know where to get the data from.
- Back end must massage external db names to eliminate artifacts of the Xref pipeline
- OLS does not seem to allow lookups of ontology accessions without an IRI. The responses to a query without an IRI are large and are not focused on a specific accession. Example: https://www.ebi.ac.uk/ols/api/ontologies/go/terms/http%253A%252F%252Fpurl.obolibrary.org%252Fobo%252FGO_0043226
- The OLS REST API behaves oddly



FOR CDM:
- Gene model needs representative_allele field
(or annotate relationship between alternative alleles with "source" and "target" fields, both of which will have to know whether either of them is representative)


Fun definitions:
Alternative alleles
Homologue: general
Orthologue: The gene in another species is kinda the same thing
    - genome_id
    - gene_id
    - similarity
    - ancestral gene?
Paralogue: The gene in this species is from the same old thing
    - gene_id
    - ...
Homeologue: The gene in this subgenome is kinda the same thing (i.e. wheat)
    - subgenome
    - gene_id
    - ...
