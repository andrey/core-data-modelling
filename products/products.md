# Number of products per transcript
If a transcript can have multiple CDSs (as is currently the case in the core model), as well as multiple products (which is highly likely), there needs to be a way to link exons to CDSs to products. One, shallow, way of doing this is to introduce foreign keys into each of these structures, such as:

```json
{
  "transcript": {
    "exons": [
      {
        "id": "exon_id_1",
        "cds_id": "cds_id_1",
        "product_id": "product_id_1"
      },
      {
        "id": "exon_id_2",
        "cds_id": "cds_id_1",
        "product_id": "product_id_1"
      }
    ],
    "splicing": [???],

    "cdss": [
      {
        "id": "cds_id_1",
        "exon_ids": ["exon_id_1", "exon_id_2"],
        "product_id": "product_id"
      }
    ],
    "products": [
      {
        "id": "product_id_1",
        "exon_ids": ["exon_id_1", "exon_id_2"],
        "cds_id": "cds_id_1"
      }
    ]
  }
}
```

# Exon boundaries

The "Protein Sequence", or "Coding exons" track in Protein view shows a sequence of rectangles, representing exons, which are separated by thin empty spaces — exon junctions:

■■■ ■■■■■■■■■■ ■■■■■■■■■■■ ■■■■■■■■■■■■

It was suggested during the REST meeting on 2020-03-27 that the coordinates for this track are given in the **protein** space (i.e. in amino acids, not in nucleotides, for the ease of alignment with protein domains). One way of representing this is by providing the length of the protein and the list of positions of splice junctions, such as:

```json
{
  "gene": {
    "transcripts": [
      {
        "product": {
          "type": "protein",
          "length": "3000",
          "splice_junction_positions": [20, 100, 500, 1500],
        }
      }
    ]
  }
}
```

alternatively, if more details about exons are required, their positional data can be represented as "splice slices", as in:

```json
{
  "product": {
    "length": "3000",
    "splice_slices": [
      {
        "location": {
          "start": 1,
          "end": 20
        }
      },
      {
        "location": {
          "start": 20,
          "end": 100
        }
      }
    ],
}
```

# Representation of positions of protein domains
Notice in the mockup of the view that:
- tracks with protein domains can be grouped together by the source of information (e.g. Superfamilies, with 3 tracks; Pfam with 4 tracks; etc.)
- a track can show multiple domains
- tracks have custom names (not exactly names used in the database; which means that the backend needs to have rules for generating "pretty names" of the tracks)

Proposed structure:
```typescript
{
  "product": {
    "protein_domains_resources": {
      "[name of resource]": {
        "name": string,
        "domains": [
          {
            "name": string,
            "source_uri": string,
            "source": {
              "name": string,
              "uri": string
            },
            "location": {
              "start": number,
              "end": number
            },
            "score": number
          },
        ]
      }
    }
  }
}
```

for example:

```json
{
  "product": {
    "protein_domains_resources": {
      "superfamilies": {
        "group_name": "Superfamilies",
        "domains": [
          {
            "name": "BRCA2 helical domain",
            "source_uri": "link to Superfamilies site?",
            "source": {
              "name": "InterProScan"
            },
            "location": {
              "start": 20,
              "end": 100
            },
            "score": 120
          },
          {
            "name": "BRCA2 helical domain",
            "source_uri": "link to Superfamilies site?",
            "source": {
              "name": "InterProScan"
            },
            "location": {
              "start": 230,
              "end": 350
            },
            "score": 105
          }
        ]
      }
    }
  }
}

```

the `name` field of the domains objects can be used to group them in a single track
