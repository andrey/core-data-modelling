# Summary of constraints

- Coding transcripts need one or more products
- Non-coding transcripts need cDNA, ordered exons, relative positions for exons, exons exclusive to this transcripts
- "product context" was used without opposition to describe post-splicing data like cds, trans-spliced exons
- trans-splicing can be implicitly described by having borrowed exons only in the product context
- Operons are not appropriate for this top-down gene-centric model and need to be described elsewhere
- Ensembl is inconsistent in the handling of pre-miRNA, mature product handling in production seems to be benched for 2020.
- Relative coordinates are essential to easy rendering, but will result in changeable content within fixed entities OR additional layers of abstraction that may be uncomfortable, such as gene->relative_thing->transcript->relative_thing->exon

`gene->relative_thing->transcript` doesn't make sense, but `transcript->relative_thing->exon` fits with our current thinking on phase and other per-transcript properties.

# Proposal
The transcript object contains two lists of exons:
  - at top level (`spliced_exons` field in the examples below)
  - within a product context (`phased_exons` field in the example below)
This allows:
  - direct access to exons from the transcript (using the top-level field)
    - no need to get to exons through products
    - there is a place to put exons even in transcripts that do not have known products
  - modelling of complex events such as trans-splicing (exons within a container for transcript products can include exons from other transcripts)
Each exon is wrapped in a container that allows combination of fixed exon properties with context-dependent properties (such as relative location, index, phase)

# Questions
1. Field names:
  - Name for the container that holds relative position of a child relative to its parent:
    - `relative_location`
    - `relative_coordinates`
    - `relative_position`
    - `relative_slice`
  - Name for the field containing exons at the root of the transcript object
    - `spliced exons`?
  - Name for the field containing products:
    - `product_generating_contexts`?
  - Name for the field containing exons within a product context
    - `phased_exons` (obviously bad, because in non-protein products there is no exon phase)
2. Do we have data where a non-coding transcript will have an RNA product? In other words, will non-coding transcripts have the `product_generating_contexts` field?
3. We are removing `splice_junction_positions` field, because the exon junctions have been removed from the design
4. If products include mature RNAs, should they have protein-related fields that are null?
5. Should api provide information about how exons map onto the protein sequence (including possible complications such as amino acid edits)? — YES, see comment 1.
6. Do we have a product-generating context for non-coding zero-product transcripts such as a lncRNA. No field = New type of transcript

# Comments
1. We will need to include information about edits
  - at the product level
  - at the transcript level


# Examples

NOTE: Values of the sequence field in the examples below are mostly bogus.

## Example 1. Coding transcript with a single exon
Bacterial gene `hemE` that spans the origin, such that its genomic end coordinate is less than its genomic start coordinate. 

```js
{
  "gene": {
    "stable_id": "RL4742",
    "symbol": "hemE",
    "transcripts": [
      {
        "stable_id": "CAK10225",
        "symbol": "hemE-1",
        "relative_location": {
          "start": 1,
          "end": 1032,
          "length": 1032
        },
        "slice": {
          "_comment": "Do we want to indicate that the region is circular?",
          "location": {
            "start": 5056183,
            "end": 72,
            "length": 1032
          },
          "region": {
            "_comment": "Length of chromosome is 5057142bp — we do not have the length field on the region according to CDM; should we?",
            "name": "Chromosome",
            "code": "chromosome",
            "strand": {
              "code": "reverse",
              "value": -1
            }
          }
        },
        "cdna": {
          "_comment": "(includes UTRS). Needs its own sequence because of possible mRNA editing",
          "length": 1032,
          "sequence": "TGACCCCCCCAGTGA",
          "sequence_checksum": "truncatedsha512"
        },
        "spliced_exons": [
          {
            "index": 0,
            "relative_location": {
              "start": 1,
              "end": 1032,
              "length": 1032
            },
            "exon": {
              "stable_id": "CAK10225-1",
              "type": "exon",
              "slice": {
                "location": {
                  "start": 5056183,
                  "end": 72,
                  "length": 1032
                },
                "region": {
                  "name": "Chromosome",
                  "code": "chromosome",
                  "strand": {
                    "code": "reverse",
                    "value": -1
                  }
                }
              }
            }
          }
        ],
        "introns": [],
        "product_generating_contexts": [
          {
            "product_type": "protein",
            "default": true,
            "cds": {
              "start": 5056183,
              "end": 72,
              "relative_start": 1,
              "relative_end": 1032,
              "protein_length": 344,
              "nucleotide_length": 1032,
              "sequence": "TGACCCCCCCAGTGA",
              "sequence_checksum": "truncatedsha512"
            },
            "5_prime_utr": null,
            "3_prime_utr": null,
            "product": {
              "stable_id": "CAK10225",
              "type": "protein",
              "length": 343,
              "sequence": "MSDTGRKIMRVLSGESLSPPPLWLMRQAGRYLPEYRETRAKAGSFLDLCYTPEHAVEVTLQPIRRYGFDAAILFSDILVIPDAMKRNVRFTEGHGPEMDPIDEAGIGSLNGEEVVDYLRPVLETVRRLREELPAETTLLGFCGAPWTVATYMIAGHGTPDQAPARLFAYKHARAFEHLLMLLADVSADYLVAQIDAGADAVQIFDSWAGVLGEKEFEAFAIRPVARMIASVKSRRPHARIIAFAKGAGYQLKTYRQKTGADAIGLDWSVPLAFAAELQKDGPVQGNLDPMRVVAGGRALEEGIDDILQHLGNGPLIFNLGHGITPQADPEHVRLLVDRVRGGA",
              "sequence_checksum": "truncatedsha512",
              "external_references": [
                {
                  "accession_id": "CAK10225",
                  "name": "CAK10225.2",
                  "description": "AM236080.1:CDS:complement(join(5056183..5057142,1..72))",
                  "source": {
                    "name": "INSDC protein ID",
                    "url": "http://www.ebi.ac.uk/ena/data/view/CAK10225"
                  }
                }
              ],
              "domains": []
            },
            "phased_exons": [
              {
                "start_phase": -1,
                "end_phase": 1,
                "index": 0,
                "relative_location": {
                  "start": 1,
                  "end": 1032,
                  "length": 1032
                },
                "exon": {
                  "stable_id": "CAK10225-1",
                  "type": "exon",
                  "slice": {
                    "location": {
                      "start": 5056183,
                      "end": 72,
                      "length": 1032
                    },
                    "region": {
                      "name": "Chromosome",
                      "code": "chromosome",
                      "strand": {
                        "code": "reverse",
                        "value": -1
                      }
                    }
                  }
                }
              }
            ]
          }
        ]
      }
    ]
  }
}
```

## Example 2. Coding transcript with several exons
BRCA2-211 (ENST00000671466.1), protein-coding transcript consisting of 2 exons.

```js
{
  "gene": {
    "stable_id": "ENSG00000139618",
    "symbol": "BRCA2",
    "transcripts": [
      {
        "stable_id": "ENST00000671466.1",
        "symbol": "BRCA2-211",
        "relative_location": {
          "start": 1,
          "end": 1442,
          "length": 1442
        },
        "slice": {
          "location": {
            "start": 32315086,
            "end": 32316527,
            "length": 1442
          },
          "region": {
            "name": "13",
            "code": "chromosome",
            "strand": {
              "code": "forward",
              "value": 1
            }
          }
        },
        "cdna": {
          "_comment": "(includes UTRS). Needs its own sequence because of possible mRNA editing",
          "length": 166,
          "sequence": "TGACCCCCCCAGTGA",
          "sequence_checksum": "truncatedsha512"
        },
        "spliced_exons": [
          {
            "index": 0,
            "relative_location": {
              "start": 1,
              "end": 60,
              "length": 60
            },
            "exon": {
              "stable_id": "ENSE00003856928",
              "type": "exon",
              "slice": {
                "location": {
                  "start": 32315086,
                  "end": 32315145,
                  "length": 60
                },
                "region": {
                  "name": "13",
                  "code": "chromosome",
                  "strand": {
                    "code": "forward",
                    "value": 1
                  }
                }
              }
            }
          },
          {
            "index": 1,
            "relative_location": {
              "start": 1337,
              "end": 1442,
              "length": 106
            },
            "exon": {
              "stable_id": "ENSE00001484009",
              "type": "exon",
              "slice": {
                "location": {
                  "start": 32316422,
                  "end": 32316527,
                  "length": 106
                },
                "region": {
                  "name": "13",
                  "code": "chromosome",
                  "strand": {
                    "code": "forward",
                    "value": 1
                  }
                }
              }
            }
          }
        ],
        "introns": [
          {
            "type": "intron",
            "index": 0,
            "relative_location": {
              "start": 61,
              "end": 1336,
              "length": 1276
            },
            "slice": {
              "location": {
                "start": 32315146,
                "end": 32316421,
                "length": 1276
              },
              "region": {
                "name": "13",
                "code": "chromosome",
                "strand": {
                  "code": "forward",
                  "value": 1
                }
              }
            }
          }
        ],
        "product_generating_contexts": [
          {
            "product_type": "protein",
            "default": true,
            "cds": {
              "start": 32316461,
              "end": 32316527,
              "relative_start": 1376,
              "relative_end": 1442,
              "protein_length": 22,
              "nucleotide_length": 66,
              "sequence": "TGACCCCCCCAGTGA",
              "sequence_checksum": "truncatedsha512"
            },
            "5_prime_utr": {
              "location": {
                "start": 32315086,
                "end": 32316461
                "length": 1376
              }
            },
            "3_prime_utr": null,
            "product": {
              "stable_id": "ENSP00000499308",
              "type": "protein",
              "length": 22,
              "sequence": "MSDTGRKIMRVLSGESLSPP",
              "sequence_checksum": "truncatedsha512",
              "external_references": [],
              "domains": []
            },
            "phased_exons": [
              {
                "start_phase": -1,
                "end_phase": -1,
                "index": 0,
                "relative_location": {
                  "start": 1,
                  "end": 60,
                  "length": 60
                },
                "exon": {
                  "stable_id": "ENSE00003856928",
                  "type": "exon",
                  "slice": {
                    "location": {
                      "start": 32315086,
                      "end": 32315145,
                      "length": 60
                    },
                    "region": {
                      "name": "13",
                      "code": "chromosome",
                      "strand": {
                        "code": "forward",
                        "value": 1
                      }
                    }
                  }
                }
              },
              {
                "start_phase": -1,
                "end_phase": -1,
                "index": 1,
                "relative_location": {
                  "start": 1337,
                  "end": 1442,
                  "length": 106
                },
                "exon": {
                  "stable_id": "ENSE00001484009",
                  "type": "exon",
                  "slice": {
                    "location": {
                      "start": 32316422,
                      "end": 32316527,
                      "length": 106
                    },
                    "region": {
                      "name": "13",
                      "code": "chromosome",
                      "strand": {
                        "code": "forward",
                        "value": 1
                      }
                    }
                  }
                }
              }
            ]
          }
        ]
      }
    ]
  }
}
```



## Example 3. Non-coding transcript
`lin-4` gene of C. elegans, which encodes microRNAs

```js
{
  "gene": {
    "stable_id": "WBGene00002993",
    "symbol": "lin-4",
    "transcripts": [
      {
        "_comment": "This is pre-mi-RNA",
        "stable_id": "F59G1.6",
        "relative_location": {
          "start": 1,
          "end": 94,
          "length": 94
        },
        "slice": {
          "location": {
            "start": 5902254,
            "end": 5902347,
            "length": 94
          },
          "region": {
            "name": "II",
            "code": "chromosome",
            "strand": {
              "code": "reverse",
              "value": -1
            }
          }
        },
        "cdna": {
          "length": 94,
          "sequence": "TGACCCCCCCAGTGA",
          "sequence_checksum": "truncatedsha512"
        },
        "spliced_exons": [
          {
            "index": 0,
            "relative_location": {
              "start": 16,
              "end": 36,
              "length": 21
            }
            "exon": {
              "stable_id": "F59G1.6a.e1",
              "type": "exon",
              "slice": {
                "location": {
                  "start": 5902269,
                  "end": 5902289,
                  "length": 21
                },
                "region": {
                  "name": "II",
                  "code": "chromosome",
                  "strand": {
                    "code": "reverse",
                    "value": -1
                  }
                }
              }
            }
          }
        ],
        "introns": [],
        "product_generating_contexts": [
          {
            "product_type": "miRNA",
            "default": true,
            "cds": null,
            "5_prime_utr": null,
            "3_prime_utr": null,
            "product": {
              "stable_id": "F59G1.6a",
              "type": "miRNA",
              "length": 21,
              "sequence": "blah",
              "sequence_checksum": "???",
              "external_references": [],
              "domains": []
            },
            "phased_exons": [
              {
                "start_phase": -1,
                "end_phase": -1,
                "index": 0,
                "relative_location": {
                  "start": 16,
                  "end": 36,
                  "length": 21
                },
                "exon": {
                  "stable_id": "F59G1.6a.e1",
                  "type": "exon",
                  "slice": {
                    "location": {
                      "start": 5902269,
                      "end": 5902289,
                      "length": 21
                    },
                    "region": {
                      "name": "II",
                      "code": "chromosome",
                      "strand": {
                        "code": "reverse",
                        "value": -1
                      }
                    }
                  }
                }
              }
            ]
          },
          {
            "product_type": "miRNA",
            "default": true,
            "cds": null,
            "5_prime_utr": null,
            "3_prime_utr": null,
            "product": {
              "stable_id": "F59G1.6b",
              "type": "miRNA",
              "length": 22,
              "sequence": "blah",
              "sequence_checksum": "???",
              "external_references": [],
              "domains": []
            },
            "phased_exons": [
              {
                "start_phase": -1,
                "end_phase": -1,
                "index": 0,
                "relative_location": {
                  "start": 54,
                  "end": 75,
                  "length": 22
                },
                "exon": {
                  "stable_id": "F59G1.6b.e1",
                  "type": "exon",
                  "slice": {
                    "location": {
                      "start": 5902308,
                      "end": 5902329,
                      "length": 22
                    },
                    "region": {
                      "name": "II",
                      "code": "chromosome",
                      "strand": {
                        "code": "reverse",
                        "value": -1
                      }
                    }
                  }
                }
              }
            ]
          }
        ]
      }
    ]
  }
}
```
