# Testing how a non-coding gene will fit into our model
## Example gene
let-7 gene from Caenorhabditis elegans, which encodes 2 microRNAs

# Comments from Kevin and Michael

- Wormbase and vertebrates model miRNAs differently
  - Wormbase has three transcripts for two products and one pre-miRNA, with pre-miRNA biotype to show the "parent"
  - Human has non-coding transcripts with biotype miRNA
- Splicing is too specific
- "if you want to "properly" do it, you probably need to allow for parent-transcripts in the transcript table in addition to parent-genes and add functionality to the API to dig down to level X (mature/pre-miRNA/ whatever biotype)."
- "miRNAs are generally not "spliced". There are processing events that converts the primary transcript (which is generally not described) into the pre_miRNA, and then the pre_miRNA into the mature products. By having the pre_miRNA and mature miRNAs as three "equal" transcripts of the gene does not reflect the biology, I agree."
- "Examples of such products include MicroRNA (miRNA), circular RNA (circRNA) or piwi-interacting RNA (piRNA), and they commonly play a role in gene expression"
- "I don't think 14 closing braces is enough!"

## Model 1: fitting the gene in the same structure as used for protein-coding genes

### Notes
- The JSON below is using the `splicing` field to represent parent-child relationships between a transcript (pre-miRNA) and a product (mature miRNAs). Here, the transcript is a pre-miRNA, and the `splicing` field contains mature miRNA. This barely works for C.elegans, but will not work for human, because we do not keep information about pre-miRNA in human database.
- Questions to the JSON below:
  - Do individual splicing structures need a `cdna` field? If mature miRNAs have a `cdna` field, then why doesn't premature miRNA have it?
  - The `default` field in individual splicing events becomes misleading
  - Should `start_phase` and `end_phase` for spliced exons be -1 or null?
- It is, of course, possible to set the whole `splicing` field to `null`
  - Then the pre-miRNA and the two miRNAs can be modelled as three independent transcripts (as Ensembl is presenting them now)
  - However, this necessitates having the `exons` field at the top level of a transcript (in the current model, exons are placed inside the `splicing` field)

```js
{
  "gene": {
    "stable_id": "WBGene00002993",
    "symbol": "lin-4",
    "transcripts": [
      {
        "_comment": "This is pre-mi-RNA",
        "stable_id": "F59G1.6",
        "relative_slice": {
          "location": {
            "start": 1,
            "end": 94,
            "length": 94
          }
        },
        "slice": {
          "location": {
            "start": 5902254,
            "end": 5902347,
            "length": 94
          },
          "region": {
            "name": "II",
            "code": "chromosome",
            "strand": {
              "code": "reverse",
              "value": -1
            }
          }
        },
        "splicing": [
          {
            "product_type": "miRNA",
            "default": true,
            "cds": null,
            "cdna": {
              "_comment": "This should be at the pre-mi-RNA level"
              "start": 5902254
              "end": 5902347,
              "relative_start": 1,
              "relative_end": 94
            },
            "product": {
              "stable_id": "F59G1.6a",
              "type": "miRNA",
              "length": 21,
              "splice_junction_positions": [],
              "sequence": "blah",
              "sequence_checksum": "???",
              "external_references": [],
              "protein_domains": []
            },
            "spliced_exons": [
              {
                "start_phase": -1,
                "end_phase": -1,
                "index": 0,
                "exon": {
                  "stable_id": "F59G1.6a.e1",
                  "type": "exon",
                  "relative_slice": {
                    "location": {
                      "start": 16,
                      "end": 36,
                      "length": 21
                    }
                  },
                  "slice": {
                    "location": {
                      "start": 5902269,
                      "end": 5902289,
                      "length": 21
                    },
                    "region": {
                      "name": "II",
                      "code": "chromosome",
                      "strand": {
                        "code": "reverse",
                        "value": -1
                      }
                    }
                  }
                }
              }
            ]
          },
          {
            "product_type": "miRNA",
            "default": true,
            "cds": null,
            "cdna": {
              "_comment": "This should be at the pre-mi-RNA level"
              "start": 5902254
              "end": 5902347,
              "relative_start": 1,
              "relative_end": 94
            },
            "product": {
              "stable_id": "F59G1.6b",
              "type": "miRNA",
              "length": 22,
              "splice_junction_positions": [],
              "sequence": "blah",
              "sequence_checksum": "???",
              "external_references": [],
              "protein_domains": []
            },
            "spliced_exons": [
              {
                "start_phase": -1,
                "end_phase": -1,
                "index": 0,
                "exon": {
                  "stable_id": "F59G1.6b.e1",
                  "type": "exon",
                  "relative_slice": {
                    "location": {
                      "start": 54,
                      "end": 75,
                      "length": 22
                    }
                  },
                  "slice": {
                    "location": {
                      "start": 5902308,
                      "end": 5902329,
                      "length": 22
                    },
                    "region": {
                      "name": "II",
                      "code": "chromosome",
                      "strand": {
                        "code": "reverse",
                        "value": -1
                      }
                    }
                  }
                }
              }
            ]
          }
        ]
      }
    ]
  }
}
```

## Model 2: non-coding transcripts are different from proteint coding transcripts and therefore have a different set of fields

### Notes
- Pre-miRNA and the two mature miRNAs are modelled here as three independent transcripts
- They do not have a `splicing` field, and therefore, do not have a `product`.
- They do have a top-level `exons` field
  - Which raises a problem with ranking of exons. There was a special `index` field in `spliced_exons` in the model with `splicing`, where we recorded the ordering of exons; but we've lost this field in the JSON below

```js
{
  "gene": {
    "stable_id": "WBGene00002993",
    "symbol": "lin-4",
    "transcripts": [
      {
        "_comment": "This is pre-mi-RNA",
        "type": "miRNA",
        "stable_id": "F59G1.6",
        "relative_slice": {
          "location": {
            "start": 1,
            "end": 94,
            "length": 94
          }
        },
        "slice": {
          "location": {
            "start": 5902254,
            "end": 5902347,
            "length": 94
          },
          "region": {
            "name": "II",
            "code": "chromosome",
            "strand": {
              "code": "reverse",
              "value": -1
            }
          }
        },
        "exons": [
          {
            "stable_id": "F59G1.6.e1",
            "type": "exon",
            "relative_slice": {
              "location": {
                "start": 1,
                "end": 94,
                "length": 94
              }
            },
            "slice": {
              "location": {
                "start": 5902254,
                "end": 5902347,
                "length": 94
              },
              "region": {
                "name": "II",
                "code": "chromosome",
                "strand": {
                  "code": "reverse",
                  "value": -1
                }
              }
            }
          }
        ]
      },
      {
        "_comment": "This is a mi-RNA",
        "type": "miRNA",
        "stable_id": "F59G1.6a",
        "relative_slice": {
          "location": {
            "start": 16,
            "end": 36,
            "length": 21
          }
        },
        "slice": {
          "location": {
            "start": 5902269,
            "end": 5902289,
            "length": 21
          },
          "region": {
            "name": "II",
            "code": "chromosome",
            "strand": {
              "code": "reverse",
              "value": -1
            }
          }
        },
        "exons": [
          {
            "stable_id": "F59G1.6a.e1",
            "type": "exon",
            "relative_slice": {
              "location": {
                "start": 1,
                "end": 21,
                "length": 21
              }
            },
            "slice": {
              "location": {
                "start": 5902269,
                "end": 5902289,
                "length": 21
              },
              "region": {
                "name": "II",
                "code": "chromosome",
                "strand": {
                  "code": "reverse",
                  "value": -1
                }
              }
            }
          }
        ]
      },
      {
        "_comment": "This is a mi-RNA",
        "type": "miRNA",
        "stable_id": "F59G1.6b",
        "relative_slice": {
          "location": {
            "start": 54,
            "end": 75,
            "length": 22
          }
        },
        "slice": {
          "location": {
            "start": 5902308,
            "end": 5902329,
            "length": 22
          },
          "region": {
            "name": "II",
            "code": "chromosome",
            "strand": {
              "code": "reverse",
              "value": -1
            }
          }
        },
        "exons": [
          {
            "stable_id": "F59G1.6a.e1",
            "type": "exon",
            "relative_slice": {
              "location": {
                "start": 1,
                "end": 22,
                "length": 22
              }
            },
            "slice": {
              "location": {
                "start": 5902308,
                "end": 5902329,
                "length": 22
              },
              "region": {
                "name": "II",
                "code": "chromosome",
                "strand": {
                  "code": "reverse",
                  "value": -1
                }
              }
            }
          }
        ]
      }
    ]
  }
}
```

## Example GraphQL queries

### For model 1 (same structure, nullable fields)

```graphql
{
    gene {
        transcripts {
            splicing {
              cds {
                relative_start
              }
              spliced_exons {
                index
                exon {
                  stable_id
                }
              }
              product {
                type
                sequence
                protein_domains
              }
            }
        }
    }
}
```

### For model 2 (polymorphic transcripts; need to resolve the union)

```graphql
{
    gene {
        transcripts {
            ...on MicroRNATranscript {
              exons {
                  stable_id
              }
            }
            ...on ProteinCodingTranscript {
                splicing {
                    cds {
                        relative_start
                    }
                }
            }
        }
    }
}
```
