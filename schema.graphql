# Notice that https://github.com/graphql/graphql-spec/issues/203 suggests using "graphql" string for extension
# Also, see in Ariadne’s repo, a feature described as "Loading schema from .graphql files."

type Query {
  gene(bySymbol: SymbolInput, byId: IdInput): Gene!
  genes(genome_id: String!): [Gene!]!
  transcripts(genome_id: String!): [Transcript!]!
  transcript(bySymbol: SymbolInput, byId: IdInput): Transcript
  slice(genome_id: String!, region: String!, start: Int!, end: Int!): Locus
}

input SymbolInput {
  symbol: String!
  genome_id: String!
}

input IdInput {
  genome_id: String!
  stable_id: String!
}


interface Feature {
  stable_id: String!
  version: Int! # notice that it’s string in CDM?
  slice: Slice! # did we decide that all features have slices?
  so_term: SO_Term
  type: String! # we probably want it as enum
}

type Gene implements Feature {
  # The commonly understood concept of gene, with its various transcriptions.
  stable_id: String!
  version: Int!
  so_term: SO_Term!
  slice: Slice!
  symbol: String!
  alternative_symbols: [String!]!
  name: String!
  transcripts: [Transcript!]!
}

type Transcript implements Feature {
  # A Transcript of a Gene. Exons are listed in sequence order
  stable_id: String!
  version: Int!
  so_term: String
  slice: Slice!
  exons: [Exon!]!
}

# NOTE: Exon does not have so_term, whereas Feature does?
type Exon {
  # An Exon that is part of a Transcript
  stable_id: String!
  type: String!
  slice: Slice!
  phase: Int!
}

type CDS {
  stable_id: String! # ENSP...?
  slice: Slice!
}

type SO_Term {
  id: String!,
  display_name: String!,
  code: String!
}

type Slice {
  """
  The container that combines Region and Location together to define a precise locus.
  The 'default' key defines whether this is the definitive locus for the parent feature.
  default:False implies there is another locus which is considered more definitive
  """
  region: Region!
  location: Location!
  default: Boolean
}

type Location {
  # A locus associated with a Region. It defines a locality within the Region perhaps corresponding to genomic feature
  start: Int!
  end: Int!
  length: Int!
  location_type: String! # ???
}

type Region {
  # A large contiguous block of assembled sequence, such as a whole chromosome.
  name: String!
  code: String! # or enum?
  strand: Strand
  assembly: String!
}

enum StrandCode {
  forward
  reverse
}

type Strand {
  # The direction of reading sense w.r.t. the assembly. 1 = 5'->3', -1 = 3'-5'
  code: StrandCode!
  value: Int!
}

type Locus {
  # A dynamically defined part of a Region
  genes: [Gene]!
  transcripts: [Transcript]!
}
